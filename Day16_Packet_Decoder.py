from math import prod


def hex_to_bin(hexadecimal):
    rules = {'0': '0000',
             '1': '0001',
             '2': '0010',
             '3': '0011',
             '4': '0100',
             '5': '0101',
             '6': '0110',
             '7': '0111',
             '8': '1000',
             '9': '1001',
             'A': '1010',
             'B': '1011',
             'C': '1100',
             'D': '1101',
             'E': '1110',
             'F': '1111'}
    binary = ''
    for c in hexadecimal:
        binary += rules[c]
    return binary


def bits_transmission(hex_as_binary, packet_version):
    if len(hex_as_binary) == 0 or all(v == '0' for v in hex_as_binary):
        return packet_version
    packet_version.append(int(hex_as_binary[0:3], 2))
    packet_type_id = int(hex_as_binary[3:6], 2)
    if packet_type_id == 4:
        i = 6
        while hex_as_binary[i] == '1':
            i += 5
        i += 5
        bits_transmission(hex_as_binary[i:], packet_version)
    else:
        i = 6
        length_type_id = hex_as_binary[i]
        i += 1
        if length_type_id == '0':
            i += 15
            bits_transmission(hex_as_binary[i:], packet_version)
        else:
            i += 11
            bits_transmission(hex_as_binary[i:], packet_version)


def bits_transmission_calculator(hex_as_binary):
    packet_type_id = int(hex_as_binary[3:6], 2)
    if packet_type_id == 4:
        i = 6
        numbers = ''
        while hex_as_binary[i] == '1':
            numbers += hex_as_binary[i+1:i+5]
            i += 5
        numbers += hex_as_binary[i + 1:i + 5]
        i += 5
        return int(numbers, 2), hex_as_binary[i:]
    if packet_type_id == 0:
        return execute_operations(hex_as_binary, sum)
    if packet_type_id == 1:
        return execute_operations(hex_as_binary, prod)
    if packet_type_id == 2:
        return execute_operations(hex_as_binary, min)
    if packet_type_id == 3:
        return execute_operations(hex_as_binary, max)
    if packet_type_id == 5:
        return execute_operations(hex_as_binary, lambda nums: 1 if nums[0] > nums[1] else 0)
    if packet_type_id == 6:
        return execute_operations(hex_as_binary, lambda nums: 1 if nums[0] < nums[1] else 0)
    if packet_type_id == 7:
        return execute_operations(hex_as_binary, lambda nums: 1 if nums[0] == nums[1] else 0)


def execute_operations(hex_as_binary, op):
    i = 6
    length_type_id = hex_as_binary[i]
    i += 1
    if length_type_id == '0':
        length_of_subpacket = int(hex_as_binary[i:i + 15], 2)
        i += 15
        sub_packet = hex_as_binary[i:i + length_of_subpacket]
        number, remaining_sub_packet = bits_transmission_calculator(sub_packet)
        numbers = [number]
        while len(remaining_sub_packet) != 0:
            number, remaining_sub_packet = bits_transmission_calculator(remaining_sub_packet)
            numbers.append(number)
        return op(numbers), hex_as_binary[i + length_of_subpacket:]
    else:
        number_of_subpackets = int(hex_as_binary[i:i + 11], 2)
        i += 11
        numbers = []
        remaining_packet = hex_as_binary[i:]
        for n in range(number_of_subpackets):
            number, remaining_packet = bits_transmission_calculator(remaining_packet)
            numbers.append(number)
        return op(numbers), remaining_packet
