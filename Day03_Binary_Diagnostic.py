import numpy as np


def life_support_rating(diagnostic_report):
    diagnostic = input_to_array(diagnostic_report)
    return calculate_co2_scrubber_rating(diagnostic)*calculate_oxygen_generator_rating(diagnostic)


def calculate_co2_scrubber_rating(diagnostic):
    return calculate_rating(diagnostic, least_common_bit)


def calculate_oxygen_generator_rating(diagnostic):
    return calculate_rating(diagnostic, most_common_bit)


def calculate_rating(diagnostic, selector, position=0):
    if len(diagnostic) == 1:
        raw = ''
        for s in diagnostic[0]:
            raw += str(s)
        return int(raw, 2)
    common = int(selector(diagnostic, position))
    new_diagnostic = [x for x in diagnostic if x[position] == common]
    return calculate_rating(new_diagnostic, selector, position+1)


def power_consumption(diagnostic_report):
    diagnostic = input_to_array(diagnostic_report)
    gamma = []
    epsilon = []
    for bit_position in range(len(diagnostic[0])):
        gamma.append(most_common_bit(diagnostic, bit_position))
        epsilon.append(least_common_bit(diagnostic, bit_position))
    gamma_rate = int(''.join(gamma), 2)
    epsilon_rate = int(''.join(epsilon), 2)
    return gamma_rate*epsilon_rate


def input_to_array(diagnostic_report):
    report = []
    for line in diagnostic_report:
        row = []
        for s in line:
            row.append(int(s))
        report.append(row)
    diagnostic = np.array(report)
    return diagnostic


def most_common_bit(diagnostic, position):
    diagnostic_sum = np.sum(diagnostic, 0)[position]
    if diagnostic_sum >= len(diagnostic)/2:
        return '1'
    else:
        return '0'


def least_common_bit(diagnostic, position):
    diagnostic_sum = np.sum(diagnostic, 0)[position]
    if diagnostic_sum >= len(diagnostic)/2:
        return '0'
    else:
        return '1'
