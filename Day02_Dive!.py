class Submarine:
    def __init__(self):
        self.depth = 0
        self.horizontal = 0

    def dive(self, instructions):
        for instruction in instructions:
            self.move(instruction)
        return self.depth*self.horizontal

    def move(self, instruction):
        [direction, units] = instruction.split()
        if direction == 'forward':
            self.move_forward(units)
        if direction == 'up':
            self.move_up(units)
        if direction == 'down':
            self.move_down(units)

    def move_down(self, units):
        self.depth += int(units)

    def move_up(self, units):
        self.depth -= int(units)

    def move_forward(self, units):
        self.horizontal += int(units)


class SubmarineWithAim(Submarine):
    def __init__(self):
        super().__init__()
        self.aim = 0

    def move_down(self, units):
        self.aim += int(units)

    def move_up(self, units):
        self.aim -= int(units)

    def move_forward(self, units):
        self.horizontal += int(units)
        self.depth += int(units) * self.aim
