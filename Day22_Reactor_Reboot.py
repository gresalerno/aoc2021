def parse_input(raw_input, initialization):
    reboot_switch = raw_input.split(' ')[0]
    raw_limits = raw_input.split(' ')[1].split(',')
    x_limits = raw_limits[0].split('=')
    y_limits = raw_limits[1].split('=')
    z_limits = raw_limits[2].split('=')
    x_min = x_limits[1].split('..')[0]
    x_max = x_limits[1].split('..')[1]
    y_min = y_limits[1].split('..')[0]
    y_max = y_limits[1].split('..')[1]
    z_min = z_limits[1].split('..')[0]
    z_max = z_limits[1].split('..')[1]
    if initialization:
        limits = [[max(-50, int(x_min)), min(50, int(x_max))],
                  [max(-50, int(y_min)), min(50, int(y_max))],
                  [max(-50, int(z_min)), min(50, int(z_max))]]
    else:
        limits = [(int(x_min), int(x_max)), (int(y_min), int(y_max)), (int(z_min), int(z_max))]
    return reboot_switch, limits


def operate_reboot(reboot_switch, limits, already_on):
    for x in range(limits[0][0], limits[0][1]+1):
        for y in range(limits[1][0], limits[1][1]+1):
            for z in range(limits[2][0], limits[2][1]+1):
                if reboot_switch == 'on':
                    already_on.add((x, y, z))
                else:
                    already_on.discard((x, y, z))
    return already_on


class Volume:
    def __init__(self, x_range, y_range, z_range):
        self.x_range = x_range
        self.y_range = y_range
        self.z_range = z_range
        self.to_remove = []

    def overlaps(self, other):
        if not (self.x_range[0] <= other.x_range[1] <= self.x_range[1] or
                other.x_range[0] <= self.x_range[1] <= other.x_range[1]):
            return False
        if not (self.y_range[0] <= other.y_range[1] <= self.y_range[1] or
                other.y_range[0] <= self.y_range[1] <= other.y_range[1]):
            return False
        if not (self.z_range[0] <= other.z_range[1] <= self.z_range[1] or
                other.z_range[0] <= self.z_range[1] <= other.z_range[1]):
            return False
        return True

    def subtract(self, other):
        if not self.overlaps(other):
            return
        x_min = max(self.x_range[0], other.x_range[0])
        x_max = min(self.x_range[1], other.x_range[1])
        y_min = max(self.y_range[0], other.y_range[0])
        y_max = min(self.y_range[1], other.y_range[1])
        z_min = max(self.z_range[0], other.z_range[0])
        z_max = min(self.z_range[1], other.z_range[1])
        overlap_volume = Volume((x_min, x_max), (y_min, y_max), (z_min, z_max))
        for v in self.to_remove:
            v.subtract(overlap_volume)
        self.to_remove.append(overlap_volume)

    def volume(self):
        volume_to_remove = sum([v.volume() for v in self.to_remove])
        return ((abs(self.x_range[1] - self.x_range[0]) + 1) * (abs(self.y_range[1] - self.y_range[0]) + 1) * (abs(
            self.z_range[1] - self.z_range[0]) + 1)) - volume_to_remove
