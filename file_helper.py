def read_file(path):
    with open(path) as file:
        return file.read().splitlines()


def read_blocks(filepath):
    lines = read_file(filepath)
    blocks = [[]]
    for line in lines:
        if len(line) != 0:
            blocks[-1].append(line)
        else:
            blocks.append([])
    return blocks
