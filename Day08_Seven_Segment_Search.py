from collections import Counter, defaultdict

NUMBERS_FOR_SEQUENCE = {
    'abcefg': '0',
    'cf': '1',
    'acdeg': '2',
    'acdfg': '3',
    'bcdf': '4',
    'abdfg': '5',
    'abdefg': '6',
    'acf': '7',
    'abcdefg': '8',
    'abcdfg': '9'
}


def count_easy_digits(four_digits_output_values):
    return len([digit for digit in four_digits_output_values.split(' | ')[1].split() if len(digit) in [2, 4, 3, 7]])


def seven_segments_search(numbers, digits):
    cipher = solve_cipher(numbers)
    correct_digits = []
    for digit in digits:
        correct_digit = []
        for segment in digit:
            correct_digit.append(cipher[segment])
        correct_digits.append(NUMBERS_FOR_SEQUENCE[''.join(sorted(correct_digit))])

    return int(''.join(correct_digits))


def solve_cipher(numbers):
    messed_displays = Counter(''.join(numbers))
    length_for_numbers = calculate_length_for_numbers(numbers)
    cipher = {list(set(length_for_numbers[3][0]) - set(length_for_numbers[2][0]))[0]: 'a'}
    for key, value in messed_displays.items():
        if value == 9:
            cipher[key] = 'f'
        if value == 6:
            cipher[key] = 'b'
        if value == 4:
            cipher[key] = 'e'
        if value == 8 and key not in cipher.keys():
            cipher[key] = 'c'
    for segment in length_for_numbers[4][0]:
        if segment not in cipher.keys():
            cipher[segment] = 'd'
    for segment in length_for_numbers[7][0]:
        if segment not in cipher.keys():
            cipher[segment] = 'g'
    return cipher


def calculate_length_for_numbers(numbers):
    length_for_numbers = defaultdict(lambda: [])
    for number in numbers:
        length_for_numbers[len(number)].append(number)
    return length_for_numbers
