def parse_array(height_map):
    height_map_array = []
    for row in height_map:
        height_map_array.append(parse_row(row))
    return height_map_array


def parse_row(height_map):
    return [int(height) for height in height_map]


def low_points(height_map):
    nx = len(height_map[0])
    ny = len(height_map)
    list_of_low_points = []
    for i in range(nx):
        for j in range(ny):
            point_candidate = height_map[j][i]
            neighbours_list = neighbours(height_map, i, j)
            if point_candidate < min(neighbours_list):
                list_of_low_points.append(point_candidate)
    return list_of_low_points


def neighbours(height_map, i, j):
    nx = len(height_map[0])
    ny = len(height_map)
    list_of_neighbours = []
    if i+1 in range(nx):
        list_of_neighbours.append(height_map[j][i+1])
    if i-1 in range(nx):
        list_of_neighbours.append(height_map[j][i - 1])
    if j + 1 in range(ny):
        list_of_neighbours.append(height_map[j+1][i])
    if j - 1 in range(ny):
        list_of_neighbours.append(height_map[j-1][i])
    return list_of_neighbours


def risk_level(list_low_points):
    return sum(list_low_points) + len(list_low_points)


def low_points_coordinates(height_map):
    nx = len(height_map[0])
    ny = len(height_map)
    list_of_low_points = []
    for i in range(nx):
        for j in range(ny):
            point_candidate = height_map[j][i]
            neighbours_list = neighbours(height_map, i, j)
            if point_candidate < min(neighbours_list):
                list_of_low_points.append((i, j))
    return list_of_low_points


def find_basin(low_point_coordinates, height_map, basin):
    nx = len(height_map[0])
    ny = len(height_map)
    x = low_point_coordinates[0]
    y = low_point_coordinates[1]
    if x >= nx or x < 0:
        return basin
    if y >= ny or y < 0:
        return basin
    if height_map[y][x] == 9:
        return basin
    if (x, y) in basin:
        return basin
    basin.append(low_point_coordinates)
    find_basin((x - 1, y), height_map, basin)
    find_basin((x + 1, y), height_map, basin)
    find_basin((x, y + 1), height_map, basin)
    find_basin((x, y - 1), height_map, basin)
    return basin
