from unittest import TestCase

from collections import defaultdict

from file_helper import read_file

from Day12 import parse_caves, explore_caves, path_filter


class TestDay12(TestCase):
    def test_explore_caves(self):
        caves = ['start-A',
                 'start-b',
                 'A-c',
                 'A-b',
                 'b-d',
                 'A-end',
                 'b-end']
        paths = []
        explore_caves(parse_caves(caves), paths, defaultdict(lambda: 0), 'start')
        self.assertEqual(10, len(paths))
        paths = []
        explore_caves(parse_caves(caves), paths, defaultdict(lambda: 0), 'start', path_filter)
        self.assertEqual(36, len(paths))

    def test_puzzle1(self):
        caves = read_file('input_day12')
        paths = []
        current_path = defaultdict(lambda: 0)
        explore_caves(parse_caves(caves), paths, current_path, 'start')
        self.assertEqual(5920, len(paths))
        paths = []
        current_path = defaultdict(lambda: 0)
        explore_caves(parse_caves(caves), paths, current_path, 'start', path_filter)
        self.assertEqual(155477, len(paths))
