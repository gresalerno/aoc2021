from unittest import TestCase

from file_helper import read_file

from Day11 import parse_octopus, tick, find_neighbours_index


class TestDay11(TestCase):
    def test_(self):
        raw_octopus = ['11111',
                       '19991',
                       '19191',
                       '19991',
                       '11111']
        self.assertEqual([[1, 1, 1, 1, 1],
                          [1, 9, 9, 9, 1],
                          [1, 9, 1, 9, 1],
                          [1, 9, 9, 9, 1],
                          [1, 1, 1, 1, 1]], parse_octopus(raw_octopus))
        octopus = parse_octopus(raw_octopus)
        self.assertEqual([(1, 0), (0, 1), (1, 1)], find_neighbours_index(0, 0, 5, 5))
        raw_octopus = ['11111',
                       '19991',
                       '19191',
                       '19991',
                       '11111']
        octopus = parse_octopus(raw_octopus)
        tick(octopus)
        self.assertEqual([[3, 4, 5, 4, 3],
                          [4, 0, 0, 0, 4],
                          [5, 0, 0, 0, 5],
                          [4, 0, 0, 0, 4],
                          [3, 4, 5, 4, 3]], octopus)
        raw_octopus = ['5483143223',
                       '2745854711',
                       '5264556173',
                       '6141336146',
                       '6357385478',
                       '4167524645',
                       '2176841721',
                       '6882881134',
                       '4846848554',
                        '5283751526']
        octopus = parse_octopus(raw_octopus)
        total_flashed = 0
        for step in range(100):
            total_flashed += tick(octopus)
        self.assertEqual(1656, total_flashed)

    def test_puzzle1(self):
        raw_octopus = read_file('input_day11')
        octopus = parse_octopus(raw_octopus)
        total_flashed = 0
        for step in range(100):
            total_flashed += tick(octopus)
        self.assertEqual(1617, total_flashed)

    def test_puzzle2(self):
        raw_octopus = read_file('input_day11')
        octopus = parse_octopus(raw_octopus)
        count = 1
        while tick(octopus) < 100:
            count += 1
        self.assertEqual(258, count)
