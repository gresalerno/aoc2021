from unittest import TestCase

from file_helper import read_file

from Day04 import Board, score, score_of_last


class TestDay04(TestCase):
    def test_board(self):
        raw_board = [[14, 21, 17, 24,  4],
                     [10, 16, 15,  9, 19],
                     [18,  8, 23, 26, 20],
                     [22, 11, 13,  6,  5],
                     [2,  0, 12,  3,  7]]
        board = Board(raw_board)
        board.extract_number(7)
        self.assertEqual([2,  0, 12,  3, 'X'], board[-1])
        list_of_numbers = [4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24]
        for number in list_of_numbers:
            board.extract_number(number)
        self.assertEqual([['X', 'X', 'X', 'X', 'X'],
                          [10, 16, 15,  'X', 19],
                          [18,  8, 'X', 26, 20],
                          [22, 'X', 13,  6, 'X'],
                          ['X', 'X', 12, 3, 'X']], board.board)
        self.assertEqual(True, board.has_won())
        self.assertEqual(188, board.calculate_sum_of_unmarked_numbers())
        board = Board(raw_board)
        list_of_numbers = [14, 10, 18, 22, 2]
        for number in list_of_numbers:
            board.extract_number(number)
        self.assertEqual(True, board.has_won())

    def test_score(self):
        board = [Board([[14, 21, 17, 24,  4],
                         [10, 16, 15,  9, 19],
                         [18,  8, 23, 26, 20],
                         [22, 11, 13,  6,  5],
                         [2,  0, 12,  3,  7]])]
        list_of_numbers = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24]
        self.assertEqual(4512, score(board, list_of_numbers))

    def test_continue_playing(self):
        boards = [Board([[14, 21, 17, 24,  4], [10, 16, 15,  9, 19], [18,  8, 23, 26, 20], [22, 11, 13,  6,  5], [2,  0, 12,  3,  7]]),
                  Board([[3, 15,  0,  2, 22], [9, 18, 13, 17,  5], [19,  8,  7, 25, 23], [20, 11, 10, 24,  4], [14, 21, 16, 12,  6]]),
                  Board([[22, 13, 17, 11,  0], [8,  2, 23,  4, 24], [21,  9, 14, 16,  7], [6, 10,  3, 18,  5], [1, 12, 20, 15, 19]])]
        list_of_numbers = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3, 26, 1]
        self.assertEqual(1924, score_of_last(boards, list_of_numbers))

    def test_puzzles(self):
        raw_input = read_file('input_day04')
        list_of_numbers = list(map(int, raw_input[0].split(',')))
        boards = []
        for index in range(2, len(raw_input), 6):
            raw_board = [parse_row(row) for row in raw_input[index:(index+5)]]
            boards.append(Board(raw_board))
        self.assertEqual(28082, score(boards, list_of_numbers))
        self.assertEqual(8224, score_of_last(boards, list_of_numbers))


def parse_row(row):
    return list(map(int, row.split()))
