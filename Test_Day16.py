from unittest import TestCase

from file_helper import read_file

from Day16 import bits_transmission, hex_to_bin, bits_transmission_calculator


class TestDay16(TestCase):
    def test_small_input(self):
        hexadecimal = 'A0016C880162017C3686B18A3D4780'
        hex_as_binary = hex_to_bin(hexadecimal)
        packet_versions = []
        bits_transmission(hex_as_binary, packet_versions)
        self.assertEqual(31, sum(packet_versions))

    def test_puzzle1(self):
        hexadecimal = read_file('input_day16')[0]
        hex_as_binary = hex_to_bin(hexadecimal)
        packet_versions = []
        bits_transmission(hex_as_binary, packet_versions)
        self.assertEqual(877, sum(packet_versions))

    def test_operands(self):
        hexadecimal = 'C200B40A82'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(3, number)
        hexadecimal = '04005AC33890'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(54, number)
        hexadecimal = '880086C3E88112'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(7, number)
        hexadecimal = 'CE00C43D881120'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(9, number)
        hexadecimal = 'F600BC2D8F'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(0, number)
        hexadecimal = '9C005AC2F8F0'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(0, number)
        hexadecimal = 'D8005AC2A8F0'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(1, number)
        hexadecimal = '9C0141080250320F1802104A08'
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(1, number)

    def test_puzzle2(self):
        hexadecimal = read_file('input_day16')[0]
        hex_as_binary = hex_to_bin(hexadecimal)
        number, _ = bits_transmission_calculator(hex_as_binary)
        self.assertEqual(194435634456, number)
