from collections import Counter


def crab_aligner(crabs, fuel_consumption_method=lambda fuel_diff: fuel_diff):
    min_crab = min(crabs)
    max_crab = max(crabs)
    min_fuel = float('+inf')
    for crab_minimum in range(min_crab, max_crab+1):
        fuel = 0
        for crab in crabs:
            fuel += fuel_consumption_method(abs(crab - crab_minimum))
        if fuel < min_fuel:
            min_fuel = fuel
    return min_fuel
