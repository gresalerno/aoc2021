from unittest import TestCase

from file_helper import read_file

from Day14 import parse_input, polymer_elements

from collections import Counter


class TestDay14(TestCase):
    def test_simple_input(self):
        raw_input = ['NNCB',
                     '',
                     'CH -> B',
                     'HH -> N',
                     'CB -> H',
                     'NH -> C',
                     'HB -> C',
                     'HC -> B',
                     'HN -> C',
                     'NN -> C',
                     'BH -> H',
                     'NC -> B',
                     'NB -> B',
                     'BN -> B',
                     'BB -> N',
                     'BC -> B',
                     'CC -> N',
                     'CN -> C']
        polymer_template, pair_insertion_rules = parse_input(raw_input)
        self.assertEqual(['N', 'N', 'C', 'B'], polymer_template)
        self.assertEqual(16, len(pair_insertion_rules))
        self.assertEqual({'N': 2, 'C': 2, 'B': 2, 'H': 1}, polymer_elements(polymer_template, pair_insertion_rules, 1))
        self.assertEqual({'N': 2, 'B': 6, 'H': 1, 'C': 4}, polymer_elements(polymer_template, pair_insertion_rules, 2))
        dic3 = Counter(
            ['N', 'B', 'B', 'B', 'C', 'N', 'C', 'C', 'N', 'B', 'B', 'N', 'B', 'N', 'B', 'B', 'C', 'H', 'B', 'H', 'H',
             'B', 'C', 'H', 'B'])
        self.assertEqual(dic3, polymer_elements(polymer_template, pair_insertion_rules, 3))
        self.assertEqual({'N': 865, 'B': 1749, 'H': 161, 'C': 298}, polymer_elements(polymer_template, pair_insertion_rules, 10))
        self.assertEqual({'N': 1096047802353, 'B': 2192039569602, 'H': 3849876073, 'C': 6597635301}, polymer_elements(polymer_template, pair_insertion_rules, 40))
        max_polymer = max(polymer_elements(polymer_template, pair_insertion_rules, 40).values())
        min_polymer = min(polymer_elements(polymer_template, pair_insertion_rules, 40).values())
        self.assertEqual(2188189693529, max_polymer - min_polymer)

    def test_puzzles(self):
        raw_input = read_file('input_day14')
        polymer_template, pair_insertion_rules = parse_input(raw_input)
        max_polymer = max(polymer_elements(polymer_template, pair_insertion_rules, 10).values())
        min_polymer = min(polymer_elements(polymer_template, pair_insertion_rules, 10).values())
        self.assertEqual(2657, max_polymer - min_polymer)
        max_polymer = max(polymer_elements(polymer_template, pair_insertion_rules, 40).values())
        min_polymer = min(polymer_elements(polymer_template, pair_insertion_rules, 40).values())
        self.assertEqual(2911561572630, max_polymer - min_polymer)
