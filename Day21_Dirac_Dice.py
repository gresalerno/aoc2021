from collections import defaultdict
from itertools import product


class Player:
    def __init__(self, space, score):
        self.space = space
        self.score = score

    def roll(self, n):
        self.space = (self.space + n + n+1 + n+2 - 1) % 10 + 1
        self.score = self.score + self.space

    def roll_dirac(self, n):
        self.space = (self.space + n - 1) % 10 + 1
        self.score = self.score + self.space


def dirac_combinations():
    combinations = product([1, 2, 3], repeat=3)
    dice_roll_sums = defaultdict(lambda: 0)
    for c in combinations:
        dice_roll_sums[sum(c)] += 1
    return dice_roll_sums


def dirac_game(space_player1, score_player1, space_player2, score_player2, turn, dice_roll_sum, memo):
    if (space_player1, score_player1, space_player2, score_player2, turn) in memo:
        return memo[(space_player1, score_player1, space_player2, score_player2, turn)]
    if score_player1 >= 21:
        memo[(space_player1, score_player1, space_player2, score_player2, turn)] = [1, 0]
        return [1, 0]
    if score_player2 >= 21:
        memo[(space_player1, score_player1, space_player2, score_player2, turn)] = [0, 1]
        return [0, 1]
    victories = [0, 0]
    for roll_sum, number_of_occurrences in dice_roll_sum.items():
        player1 = Player(space_player1, score_player1)
        player2 = Player(space_player2, score_player2)
        if turn == 'player1':
            player1.roll_dirac(roll_sum)
            roll_sum_victories = dirac_game(player1.space, player1.score, space_player2, score_player2, 'player2',
                                            dice_roll_sum, memo)
        else:
            player2.roll_dirac(roll_sum)
            roll_sum_victories = dirac_game(space_player1, score_player1, player2.space, player2.score, 'player1',
                                            dice_roll_sum, memo)
        victories[0] += roll_sum_victories[0] * number_of_occurrences
        victories[1] += roll_sum_victories[1] * number_of_occurrences
    memo[(space_player1, score_player1, space_player2, score_player2, turn)] = victories
    return victories


def deterministic_game(space_player1, score_player1, space_player2, score_player2):
    player1 = Player(space_player1, score_player1)
    player2 = Player(space_player2, score_player2)
    n = 1
    dice_rolled = 0
    while player1.score < 1000 and player2.score < 1000:
        player1.roll(n)
        dice_rolled += 3
        if player1.score < 1000:
            n = n + 3
            player2.roll(n)
            dice_rolled += 3
            n = n + 3
    if player1.score >= 1000:
        deterministic = dice_rolled * player2.score
    else:
        deterministic = dice_rolled * player1.score
    return deterministic
