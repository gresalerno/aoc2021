from collections import Counter, defaultdict


def parse_input(raw_input):
    polymer_template_raw = raw_input[0]
    polymer_template = []
    for letter in polymer_template_raw:
        polymer_template.append(letter)
    pair_insertion_rules = {}
    for line in raw_input[2:]:
        [pair, insertion] = line.split(' -> ')
        rule = []
        for p in pair:
            rule.append(p)
        inserted_rule_left = (rule[0], insertion)
        inserted_rule_right = (insertion, rule[1])
        pair_insertion_rules[tuple(pair)] = [inserted_rule_left, inserted_rule_right]
    return polymer_template, pair_insertion_rules


def polymer_elements(polymer_template, pair_insertion_rules, times):
    pair_of_elements = Counter(zip(polymer_template, polymer_template[1:]))
    elements = Counter(polymer_template)
    for t in range(times):
        pair_of_elements_new = defaultdict(lambda: 0)
        for couple in pair_of_elements.keys():
            for inserted_pair in pair_insertion_rules[couple]:
                pair_of_elements_new[inserted_pair] += pair_of_elements[couple]
            elements[pair_insertion_rules[couple][0][1]] += pair_of_elements[couple]
        pair_of_elements = pair_of_elements_new
    return elements
