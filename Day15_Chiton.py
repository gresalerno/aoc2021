from collections import defaultdict
from queue import PriorityQueue
import numpy as np


def parse_input(raw_input):
    risk_level_map = []
    for line in raw_input:
        risk_level_map.append([int(n) for n in line])
    return risk_level_map


def create_bigger_risk_level_map(risk_level_map):
    lx = len(risk_level_map[0])
    ly = len(risk_level_map)
    small_matrix = np.array(risk_level_map)
    to_add = np.array([list(range(5)), list(range(1, 6)),
                      list(range(2, 7)), list(range(3, 8)), list(range(4, 9))])
    total_matrix = np.kron(np.ones((5, 5)), small_matrix) + np.kron(to_add, np.ones((lx, ly)))
    total_matrix[total_matrix > 9] -= 9
    return total_matrix


def create_graph(risk_level_map):
    lx = len(risk_level_map[0])
    ly = len(risk_level_map)
    graph = defaultdict(lambda: {})
    for y in range(ly):
        for x in range(lx):
            neighbours = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
            for neighbour in neighbours:
                if neighbour[0] in range(lx) and neighbour[1] in range(ly):
                    graph[(x, y)][neighbour] = risk_level_map[neighbour[1]][neighbour[0]]
    return graph, lx, ly


def move(graph, lx, ly, start=(0, 0)):
    risk_levels = {}
    visited = set(start)
    queue = PriorityQueue()
    queue.put((0, start))
    for v in graph.keys():
        risk_levels[v] = float('inf')
    risk_levels[start] = 0
    while not queue.empty():
        current_risk, current_vertex = queue.get()
        if current_vertex == (lx, ly):
            return risk_levels[current_vertex]
        neighbours = graph[current_vertex].keys()
        visited.add(current_vertex)
        for n in neighbours:
            if n in visited:
                continue
            neighbour_current_risk = risk_levels[n]
            new_risk = current_risk + graph[current_vertex][n]
            if new_risk < neighbour_current_risk:
                risk_levels[n] = new_risk
                queue.put((new_risk, n))
    return risk_levels
