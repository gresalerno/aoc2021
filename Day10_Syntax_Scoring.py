def find_illegal_character(line, stash):
    opening = ['(', '[', '{', '<']
    closing = [')', ']', '}', '>']
    for char in line:
        if char in opening:
            stash.append(char)
        if char in closing:
            last_open = stash.pop()
            if closing.index(char) != opening.index(last_open):
                return char


def calculate_syntax_error_score(line):
    char = find_illegal_character(line, [])
    if char is None:
        score = 0
    if char == ')':
        score = 3
    if char == ']':
        score = 57
    if char == '}':
        score = 1197
    if char == '>':
        score = 25137
    return score


def calculate_autocomplete_score(line):
    opening = ['(', '[', '{', '<']
    stash = []
    if find_illegal_character(line, stash) is not None:
        return None
    stash.reverse()
    score = 0
    for char in stash:
        score = score*5 + (opening.index(char)+1)
    return score
