def parse_octopus(raw_octopus):
    octopus = []
    for row in raw_octopus:
        octopus.append(list(map(int, [o for o in row])))
    return octopus


def tick(octopus):
    ny = len(octopus)
    nx = len(octopus[0])
    already_flashed = []
    points_to_flash = []
    tot_flashed = 0
    for i in range(nx):
        for j in range(ny):
            if (i, j) not in already_flashed:
                octopus[j][i] += 1
                if octopus[j][i] == 10:
                    points_to_flash.append((i, j))
                    octopus[j][i] = 0
                    already_flashed.append((i, j))
                    tot_flashed += 1
                    while len(points_to_flash) > 0:
                        point = points_to_flash.pop(0)
                        neighbours_index = find_neighbours_index(point[0], point[1], nx, ny)
                        for n in neighbours_index:
                            if n not in already_flashed:
                                octopus[n[1]][n[0]] += 1
                                if octopus[n[1]][n[0]] == 10:
                                    octopus[n[1]][n[0]] = 0
                                    points_to_flash.append(n)
                                    tot_flashed += 1
                                    already_flashed.append(n)
    return tot_flashed


def find_neighbours_index(i, j, nx, ny):
    candidate_neighbours = [(i+1, j), (i+1, j-1), (i, j-1), (i-1, j-1), (i-1, j), (i-1, j+1), (i, j+1), (i+1, j+1)]
    neighbours_index = []
    for n in candidate_neighbours:
        if n[0] in range(nx) and n[1] in range(ny):
            neighbours_index.append(n)
    return neighbours_index
