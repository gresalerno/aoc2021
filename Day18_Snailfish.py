from math import floor, ceil


class Pair:
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __eq__(self, other):
        return self.left == other.left and self.right == other.right

    def __repr__(self):
        return f"[{self.left},{self.right}]"

    def __add__(self, other):
        return Pair(self, other)

    def magnitude(self):
        return 3 * self.left.magnitude() + 2 * self.right.magnitude()


class Value:
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return self.value == other.value

    def __repr__(self):
        return f"{self.value}"

    def magnitude(self):
        return self.value


def split_number(number):
    if type(number.left) == Value and number.left.value >= 10:
        regular_number = number.left.value
        number.left = Pair(Value(floor(regular_number/2)), Value(ceil(regular_number/2)))
        return True
    if type(number.left) == Pair and split_number(number.left):
        return True
    if type(number.right) == Value and number.right.value >= 10:
        regular_number = number.right.value
        number.right = Pair(Value(floor(regular_number/2)), Value(ceil(regular_number/2)))
        return True
    if type(number.right) == Pair and split_number(number.right):
        return True
    return False


def next_explosion(pair, current_path):
    if type(pair) == Value:
        return None
    if len(current_path) == 4:
        return current_path, pair
    candidate = next_explosion(pair.left, current_path + ['left'])
    if candidate is not None:
        return candidate
    return next_explosion(pair.right, current_path + ['right'])


def explode(pair, path, root):
    handle_left(pair, path, root)
    handle_right(pair, path, root)
    value_to_add = root
    for t in path[:-1]:
        if t == 'left':
            value_to_add = value_to_add.left
        else:
            value_to_add = value_to_add.right
    if path[-1] == 'left':
        value_to_add.left = Value(0)
    else:
        value_to_add.right = Value(0)


def handle_right(pair, path, root):
    sub_path = last_turn(path, 'left')
    if sub_path is None:
        return
    last = sub_path[:-1] + ['right']
    value_to_add = root
    for t in last:
        if t == 'left':
            value_to_add = value_to_add.left
        else:
            value_to_add = value_to_add.right
    while type(value_to_add) != Value:
        value_to_add = value_to_add.left
    value_to_add.value += pair.right.value


def handle_left(pair, path, root):
    sub_path = last_turn(path, 'right')
    if sub_path is None:
        return
    last = sub_path[:-1] + ['left']
    value_to_add = root
    for t in last:
        if t == 'left':
            value_to_add = value_to_add.left
        else:
            value_to_add = value_to_add.right
    while type(value_to_add) != Value:
        value_to_add = value_to_add.right
    value_to_add.value += pair.left.value


def last_turn(path, position):
    try:
        index = len(path) - 1 - path[::-1].index(position)
        return path[:index + 1]
    except:
        return None


def reduce_number(number):
    while True:
        while True:
            explosion = next_explosion(number, [])
            if explosion is None:
                break
            path, pair = explosion
            explode(pair, path, number)
        if not split_number(number):
            return


def add_raw_numbers(raw_numbers):
    number = parse_input(eval(raw_numbers[0]))
    reduce_number(number)
    for raw_number in raw_numbers[1:]:
        addend = parse_input(eval(raw_number))
        number = number + addend
        reduce_number(number)
    return number


def find_largest_magnitude(raw_numbers):
    highest_max_magnitude = float('-inf')
    for raw_number in raw_numbers:
        remaining_raw_numbers = list(raw_numbers)
        remaining_raw_numbers.remove(raw_number)
        max_magnitude = float('-inf')
        for other_raw_number in remaining_raw_numbers:
            number = parse_input(eval(raw_number))
            reduce_number(number)
            summed_numbers = number + parse_input(eval(other_raw_number))
            reduce_number(summed_numbers)
            magnitude = summed_numbers.magnitude()
            if magnitude > max_magnitude:
                max_magnitude = magnitude
        if max_magnitude > highest_max_magnitude:
            highest_max_magnitude = max_magnitude
    return highest_max_magnitude


def parse_input(number):
    left, right = number
    if type(right) == int:
        right_node = Value(right)
    else:
        right_node = parse_input(right)
    if type(left) == int:
        left_node = Value(left)
    else:
        left_node = parse_input(left)
    return Pair(left_node, right_node)
