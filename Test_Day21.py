from unittest import TestCase

from Day21 import deterministic_game, dirac_combinations, dirac_game


class TestDay21(TestCase):
    def test_simple_input(self):
        space_player1 = 4
        space_player2 = 8
        score_player1, score_player2 = 0, 0
        deterministic = deterministic_game(space_player1, score_player1, space_player2, score_player2)
        self.assertEqual(739785, deterministic)

    def test_puzzle1(self):
        space_player1 = 10
        space_player2 = 7
        score_player1, score_player2 = 0, 0
        deterministic = deterministic_game(space_player1, score_player1, space_player2, score_player2)
        self.assertEqual(906093, deterministic)

    def test_dirac_game_simple(self):
        space_player1 = 4
        space_player2 = 8
        score_player1, score_player2 = 0, 0
        dice_roll_sums = dirac_combinations()
        victories = dirac_game(space_player1, score_player1, space_player2, score_player2, 'player1', dice_roll_sums, {})
        self.assertEqual([444356092776315, 341960390180808], victories)

    def test_puzzle2(self):
        space_player1 = 10
        space_player2 = 7
        score_player1, score_player2 = 0, 0
        dice_roll_sums = dirac_combinations()
        victories = dirac_game(space_player1, score_player1, space_player2, score_player2, 'player1', dice_roll_sums,
                               {})
        self.assertEqual([274291038026362, 135620348907779], victories)
        self.assertEqual(274291038026362, max(victories))
