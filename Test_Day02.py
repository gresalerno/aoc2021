from unittest import TestCase

from file_helper import read_file

from Day02 import Submarine, SubmarineWithAim


class TestDay02(TestCase):
    def test_(self):
        instructions = ('forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2')
        self.assertEqual(150, Submarine().dive(instructions))
        instructions_puzzle = read_file('input_day02')
        self.assertEqual(1480518, Submarine().dive(instructions_puzzle))
        self.assertEqual(900, SubmarineWithAim().dive(instructions))
        self.assertEqual(1282809906, SubmarineWithAim().dive(instructions_puzzle))
