from unittest import TestCase

from file_helper import read_file

from Day17 import calculate_new_position, parse_input, calculate_valid_velocities


class TestDay17(TestCase):
    def test_small_input(self):
        x, y = 0, 0
        vx, vy = 7, 2
        raw_input = 'target area: x=20..30, y=-10..-5'
        xmin, xmax, ymin, ymax = parse_input(raw_input)
        self.assertEqual(20, xmin)
        self.assertEqual(30, xmax)
        self.assertEqual(-10, ymin)
        self.assertEqual(-5, ymax)
        for _ in range(7):
            x, y, vx, vy = calculate_new_position(x, y, vx, vy)
        self.assertEqual(28, x)
        self.assertEqual(-7, y)
        self.assertEqual(0, vx)
        self.assertEqual(-5, vy)
        max_height, number_of_valid_trajectories = calculate_valid_velocities(xmin, xmax, ymin, ymax)
        self.assertEqual(45, max_height)
        self.assertEqual(112, number_of_valid_trajectories)

    def test_puzzles(self):
        raw_input = 'target area: x=29..73, y=-248..-194'
        xmin, xmax, ymin, ymax = parse_input(raw_input)
        max_height, number_of_valid_trajectories = calculate_valid_velocities(xmin, xmax, ymin, ymax)
        self.assertEqual(30628, max_height)
        self.assertEqual(4433, number_of_valid_trajectories)

    def test_puzzle_matteo(self):
        raw_input = 'target area: x=32..65, y=-225..-177'
        xmin, xmax, ymin, ymax = parse_input(raw_input)
        max_height, number_of_valid_trajectories = calculate_valid_velocities(xmin, xmax, ymin, ymax)
        self.assertEqual(25200, max_height)
        self.assertEqual(3012, number_of_valid_trajectories)
