from unittest import TestCase

from file_helper import read_blocks

from Day19 import parse_input, find_scanner_origin, rotate, difference, get_all_rotated_scanners, \
    find_scanners_positions, find_unique_beacons, calculate_max_distance


class TestDay19(TestCase):
    def test_distances(self):
        raw_scanners = [['--- scanner 0 ---', '0,2,0', '4,1,0', '3,3,0'],
                        ['--- scanner 1 ---', '-1,-1,0', '-5,0,0', '-2,1,0']]
        scanners = parse_input(raw_scanners)
        origin = find_scanner_origin(scanners[0], scanners[1], 3)
        self.assertEqual([5, 2, 0], origin)

    def test_rotate(self):
        raw_scanners = [['--- scanner 0 ---', '0,2,0', '4,1,0', '3,3,0'],
                        ['--- scanner 1 ---', '1,2,3']]
        scanners = parse_input(raw_scanners)
        all_scanners = [[[1, 2, 3]], [[1, 2, -3]], [[1, -2, 3]], [[1, -2, -3]],
                        [[-1, 2, 3]], [[-1, 2, -3]], [[-1, -2, 3]], [[-1, -2, -3]],
                        [[2, 1, 3]], [[2, -1, 3]], [[2, 1, -3]], [[2, -1, -3]],
                        [[-2, 1, 3]], [[-2, -1, 3]], [[-2, 1, -3]], [[-2, -1, -3]],
                        [[3, 1, 2]], [[3, 1, -2]], [[3, -1, 2]], [[3, -1, -2]],
                        [[-3, 1, 2]], [[-3, 1, -2]], [[-3, -1, 2]], [[-3, -1, -2]]]
        print(get_all_rotated_scanners(scanners[1].beacons_original_positions))
        self.assertEqual(all_scanners, rotate(scanners[1].beacons_original_positions))
        self.assertEqual(24, len(rotate(scanners[1].beacons_original_positions)))

    def test_small_input(self):
        raw_scanners = read_blocks('input_day19_test')
        scanners = parse_input(raw_scanners)
        find_scanners_positions(scanners)
        self.assertEqual([68, -1246, -43], scanners[1].absolute_position)
        self.assertEqual([-20, -1133, 1061], scanners[4].absolute_position)
        self.assertEqual([-92, -2380, -20], scanners[3].absolute_position)
        self.assertEqual([1105, -1205, 1229], scanners[2].absolute_position)
        self.assertEqual(79, find_unique_beacons(scanners))
        self.assertEqual(3621, calculate_max_distance(scanners))

    def test_puzzles(self):
        raw_scanners = read_blocks('input_day19')
        scanners = parse_input(raw_scanners)
        find_scanners_positions(scanners)
        self.assertEqual(385, find_unique_beacons(scanners))
        self.assertEqual(10707, calculate_max_distance(scanners))

    def test_matteo_puzzles(self):
        raw_scanners = read_blocks('input_matteo_day19')
        scanners = parse_input(raw_scanners)
        find_scanners_positions(scanners)
        self.assertEqual(376, find_unique_beacons(scanners))
        self.assertEqual(10772, calculate_max_distance(scanners))