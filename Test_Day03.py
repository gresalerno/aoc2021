from unittest import TestCase

from file_helper import read_file

from Day03 import power_consumption, life_support_rating


class TestDay03(TestCase):
    def test_(self):
        diagnostic_report = ('00100', '11110', '10110', '10111', '10101', '01111',
                             '00111', '11100', '10000', '11001', '00010', '01010')
        self.assertEqual(198, power_consumption(diagnostic_report))
        puzzle = read_file('input_day03')
        self.assertEqual(3320834, power_consumption(puzzle))
        self.assertEqual(230, life_support_rating(diagnostic_report))
        self.assertEqual(4481199, life_support_rating(puzzle))
