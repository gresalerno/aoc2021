from unittest import TestCase

from file_helper import read_file

from Day07 import crab_aligner


class TestDay07(TestCase):
    def test(self):
        crabs = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
        self.assertEqual(37, crab_aligner(crabs))
        self.assertEqual(168, crab_aligner(crabs, lambda fuel_diff: fuel_diff*(fuel_diff+1)/2))

    def test_puzzle1(self):
        raw_input = read_file('input_day07')
        crabs = list(map(int, raw_input[0].split(',')))
        self.assertEqual(352331, crab_aligner(crabs))

    def test_puzzle2(self):
        raw_input = read_file('input_day07')
        crabs = list(map(int, raw_input[0].split(',')))
        self.assertEqual(99266250, crab_aligner(crabs, lambda fuel_diff: fuel_diff*(fuel_diff+1)/2))
