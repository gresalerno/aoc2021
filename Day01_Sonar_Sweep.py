def increase_depths_measures(depths):
    return len(list(filter(lambda t: t[0] < t[1], zip(depths, depths[1:]))))


def increase_three_measurement(depths):
    triplets = list(map(sum, zip(depths, depths[1:], depths[2:])))
    return increase_depths_measures(triplets)
