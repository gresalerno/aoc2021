from collections import Counter


def evolve(population, iterations=1):
    timer_occurrences = Counter(population)
    for _ in range(iterations):
        first_timer = timer_occurrences[0]
        for timer_index in range(9):
            timer_occurrences[timer_index] = timer_occurrences[timer_index + 1]
        timer_occurrences[6] += first_timer
        timer_occurrences[8] += first_timer
    return timer_occurrences
