from unittest import TestCase

from queue import PriorityQueue

from Day23 import calculate_path, distance, best_cost, best_cost_extended


class TestDay23(TestCase):
    def test_distance(self):
        self.assertEqual(3, distance((0, 0), (2, 1)))
        self.assertEqual(6, distance((1, 0), (6, 1)))
        self.assertEqual(2, distance((8, 1), (9, 0)))
        self.assertEqual({(9, 0), (8, 0)}, calculate_path((8, 1), (9, 0)))
        amphipod_positions = {((4, 1), 'C'), ((3, 0), 'A'), ((5, 0), 'B'), ((2, 1), 'B'), ((9, 0), 'D'), ((4, 2), 'D'),
                              ((6, 2), 'C'), ((2, 2), 'A')}
        path = calculate_path((4, 1), (6, 1))
        print(path)
        self.assertTrue(any(p in path for (p, _) in amphipod_positions))

    def test_calculate_path(self):
        self.assertEqual({(1, 0), (2, 0), (2, 1)}, calculate_path((0, 0), (2, 1)))
        self.assertEqual({(1, 0), (2, 0), (0, 0)}, calculate_path((2, 1), (0, 0)))
        self.assertEqual({(2, 2)}, calculate_path((2, 1), (2, 2)))
        self.assertEqual({(2, 1)}, calculate_path((2, 2), (2, 1)))
        self.assertEqual({(9, 0), (4, 0), (0, 0), (7, 0), (2, 0), (8, 0), (3, 0), (5, 0), (6, 0), (1, 0)},
                         calculate_path((10, 0), (0, 0)))
        self.assertEqual({(4, 0), (2, 1), (4, 1), (2, 0), (3, 0)}, calculate_path((2, 2), (4, 1)))
        self.assertEqual({(2, 0), (4, 0), (2, 1), (3, 0)}, calculate_path((4, 1), (2, 1)))
        self.assertEqual({(6, 2), (4, 0), (6, 1), (2, 0), (3, 0), (5, 0), (6, 0)}, calculate_path((1, 0), (6, 2)))

    def test_best_cost(self):
        queue = PriorityQueue()
        amphipod_positions = {
            ((2, 1), 'B'), ((2, 2), 'A'),
            ((4, 1), 'C'), ((4, 2), 'D'),
            ((6, 1), 'B'), ((6, 2), 'C'),
            ((8, 1), 'D'), ((8, 2), 'A')}
        queue.put((0, amphipod_positions, []))
        cost, pp = best_cost(queue, set())
        acc = amphipod_positions
        for p in pp:
            print(f"{acc - p} -> {p - acc}")
            acc = p
            print()
        self.assertEqual(12521, cost)

    def test_puzzle1(self):
        queue = PriorityQueue()
        amphipod_positions = {
            ((2, 1), 'B'), ((2, 2), 'C'),
            ((4, 1), 'C'), ((4, 2), 'D'),
            ((6, 1), 'A'), ((6, 2), 'D'),
            ((8, 1), 'B'), ((8, 2), 'A')}
        queue.put((0, amphipod_positions, []))
        cost, pp = best_cost(queue, set())
        self.assertEqual(14350, cost)

    def test_puzzle2(self):
        queue = PriorityQueue()
        amphipod_positions = {
            ((2, 1), 'B'), ((2, 2), 'D'), ((2, 3), 'D'), ((2, 4), 'C'),
            ((4, 1), 'C'), ((4, 2), 'C'), ((4, 3), 'B'), ((4, 4), 'D'),
            ((6, 1), 'A'), ((6, 2), 'B'), ((6, 3), 'A'), ((6, 4), 'D'),
            ((8, 1), 'B'), ((8, 2), 'A'), ((8, 3), 'C'), ((8, 4), 'A')}
        queue.put((0, amphipod_positions, []))
        cost, pp = best_cost_extended(queue, set())
        self.assertEqual(49742, cost)

    def test_puzzle2matteo(self):
        queue = PriorityQueue()
        amphipod_positions = {
            ((2, 1), 'A'), ((2, 2), 'D'), ((2, 3), 'D'), ((2, 4), 'C'),
            ((4, 1), 'D'), ((4, 2), 'C'), ((4, 3), 'B'), ((4, 4), 'D'),
            ((6, 1), 'C'), ((6, 2), 'B'), ((6, 3), 'A'), ((6, 4), 'B'),
            ((8, 1), 'A'), ((8, 2), 'A'), ((8, 3), 'C'), ((8, 4), 'B')}
        queue.put((0, amphipod_positions, []))
        cost, pp = best_cost_extended(queue, set())
        self.assertEqual(49742, cost)
