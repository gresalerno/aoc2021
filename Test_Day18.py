from unittest import TestCase

from file_helper import read_file

from Day18 import Pair, Value, parse_input, split_number, add_raw_numbers, find_largest_magnitude


class TestDay18(TestCase):
    def test_magnitude(self):
        self.assertEqual(29, Pair(Value(9), Value(1)).magnitude())
        number = eval('[[1, 2], [[3, 4], 5]]')
        snailfishnumber = Pair(Pair(Value(1), Value(2)), Pair(Pair(Value(3), Value(4)), Value(5)))
        number_snail = parse_input(number)
        self.assertEqual(snailfishnumber, number_snail)
        self.assertEqual(143, number_snail.magnitude())
        self.assertEqual(143, snailfishnumber.magnitude())

    def test_split(self):
        number = eval('[[[[0,7],4],[15,[0,13]]],[1,1]]')
        number_snail = parse_input(number)
        number_snail_splitted = split_number(number_snail)
        self.assertEqual('[[[[0,7],4],[[7,8],[0,13]]],[1,1]]', str(number_snail))
        split_number(number_snail)
        self.assertEqual('[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]', str(number_snail))

    def test_sum_number(self):
        raw_numbers = ['[1, 1]',
                       '[2, 2]',
                       '[3, 3]',
                       '[4, 4]']
        number = add_raw_numbers(raw_numbers)
        self.assertEqual('[[[[1,1],[2,2]],[3,3]],[4,4]]', str(number))

        raw_numbers = ['[[[0, [4, 5]], [0, 0]], [[[4, 5], [2, 6]], [9, 5]]]',
                       '[7, [[[3, 7], [4, 3]], [[6, 3], [8, 8]]]]',
                       '[[2, [[0, 8], [3, 4]]], [[[6, 7], 1], [7, [1, 6]]]]',
                       '[[[[2, 4], 7], [6, [0, 5]]], [[[6, 8], [2, 8]], [[2, 1], [4, 5]]]]',
                       '[7, [5, [[3, 8], [1, 4]]]]',
                       '[[2, [2, 2]], [8, [8, 1]]]',
                       '[2, 9]',
                       '[1, [[[9, 3], 9], [[9, 0], [0, 7]]]]',
                       '[[[5, [7, 4]], 7], 1]',
                       '[[[[4, 2], 2], 6], [8, 7]]']
        number = add_raw_numbers(raw_numbers)
        self.assertEqual('[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]', str(number))
        self.assertEqual(3488, number.magnitude())

    def test_highest_magnitude(self):
        raw_numbers = ['[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]',
                       '[[[5,[2,8]],4],[5,[[9,9],0]]]',
                       '[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]',
                       '[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]',
                       '[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]',
                       '[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]',
                       '[[[[5,4],[7,7]],8],[[8,3],8]]',
                       '[[9,3],[[9,9],[6,[4,9]]]]',
                       '[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]',
                       '[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]']
        highest_magnitude = find_largest_magnitude(raw_numbers)
        self.assertEqual(3993, highest_magnitude)

    def test_puzzles(self):
        raw_numbers = read_file('input_day18')
        number = add_raw_numbers(raw_numbers)
        self.assertEqual(4176, number.magnitude())
        highest_magnitude = find_largest_magnitude(raw_numbers)
        self.assertEqual(4633, highest_magnitude)
