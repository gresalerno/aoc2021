from unittest import TestCase

from file_helper import read_file

from Day20 import parse_image, tick


class TestDay20(TestCase):
    def test_parse(self):
        raw_image = [
            '..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#',
            '',
            '#..#.',
            '#....',
            '##..#',
            '..#..',
            '..###']
        algorithm, image, max_x, max_y = parse_image(raw_image)
        self.assertEqual(10, len(image))
        offset = 4
        new_image = tick(algorithm, image, max_x, max_y, offset)
        offset -= 2
        new_image = tick(algorithm, new_image, max_x, max_y, offset)
        self.assertEqual(35, len(new_image))
        for i in range(24):
            offset += 4
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
            offset -= 2
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
        self.assertEqual(3351, len(new_image))

    def test_puzzle1(self):
        raw_image = read_file('input_day20')
        algorithm, new_image, max_x, max_y = parse_image(raw_image)
        offset = 0
        for i in range(1):
            offset += 4
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
            offset -= 2
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
        self.assertEqual(5680, len(new_image))

    def test_puzzle2(self):
        raw_image = read_file('input_day20')
        algorithm, new_image, max_x, max_y = parse_image(raw_image)
        offset = 0
        for i in range(25):
            offset += 4
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
            offset -= 2
            new_image = tick(algorithm, new_image, max_x, max_y, offset)
        self.assertEqual(19766, len(new_image))
