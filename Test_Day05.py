from unittest import TestCase

from file_helper import read_file

from Day05 import Line, parse_line, calculate_multiple_occurrences, \
    calculate_multiple_occurrences_in_horizontal_or_vertical_lines


class TestDay05(TestCase):
    def test_points_in_line(self):
        line = Line((0, 9), (5, 9))
        self.assertEqual([(0, 9), (1, 9), (2, 9), (3, 9), (4, 9), (5, 9)], line.all_points())
        line = Line((5, 5), (5, 0))
        self.assertEqual([(5, 0), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5)], line.all_points())

    def test_horizontal_and_vertical_lines(self):
        lines = [Line((0, 9), (5, 9)),
                 Line((8, 0), (0, 8)),
                 Line((9, 4), (3, 4)),
                 Line((2, 2), (2, 1)),
                 Line((7, 0), (7, 4)),
                 Line((6, 4), (2, 0)),
                 Line((0, 9), (2, 9)),
                 Line((3, 4), (1, 4)),
                 Line((0, 0), (8, 8)),
                 Line((5, 5), (8, 2))]
        horizontal_or_vertical_lines = [line for line in lines if line.is_horizontal_or_vertical()]
        self.assertEqual([Line((0, 9), (5, 9)), Line((9, 4), (3, 4)), Line((2, 2), (2, 1)),
                          Line((7, 0), (7, 4)), Line((0, 9), (2, 9)), Line((3, 4), (1, 4))],
                         horizontal_or_vertical_lines)

    def test_number_of_lines_crossing(self):
        lines = [Line((0, 9), (5, 9)),
                 Line((8, 0), (0, 8)),
                 Line((9, 4), (3, 4)),
                 Line((2, 2), (2, 1)),
                 Line((7, 0), (7, 4)),
                 Line((6, 4), (2, 0)),
                 Line((0, 9), (2, 9)),
                 Line((3, 4), (1, 4)),
                 Line((0, 0), (8, 8)),
                 Line((5, 5), (8, 2))]
        number_of_multiple_occurrences_horvert = calculate_multiple_occurrences_in_horizontal_or_vertical_lines(lines)
        self.assertEqual(5, number_of_multiple_occurrences_horvert)
        lines = [Line((0, 9), (5, 9)),
                 Line((8, 0), (0, 8)),
                 Line((9, 4), (3, 4)),
                 Line((2, 2), (2, 1)),
                 Line((7, 0), (7, 4)),
                 Line((6, 4), (2, 0)),
                 Line((0, 9), (2, 9)),
                 Line((3, 4), (1, 4)),
                 Line((0, 0), (8, 8)),
                 Line((5, 5), (8, 2))]
        number_of_multiple_occurrences = calculate_multiple_occurrences(lines)
        self.assertEqual(12, number_of_multiple_occurrences)

    def test_parse_input(self):
        raw_input = '424,924 -> 206,706'
        self.assertEqual(Line((424, 924), (206, 706)), parse_line(raw_input))

    def test_puzzle1(self):
        raw_input = read_file('input_day05')
        lines = [parse_line(raw_line) for raw_line in raw_input]
        number_of_multiple_occurrences_horvert = calculate_multiple_occurrences_in_horizontal_or_vertical_lines(lines)
        self.assertEqual(5147, number_of_multiple_occurrences_horvert)

    def test_diagonal(self):
        line = Line((1, 1), (3, 3))
        self.assertEqual([(1, 1), (2, 2), (3, 3)], line.all_points())
        line = Line((9, 7), (7, 9))
        self.assertEqual([(7, 9), (8, 8), (9, 7)], line.all_points())
        line = Line((7, 9), (9, 7))
        self.assertEqual([(7, 9), (8, 8), (9, 7)], line.all_points())

    def test_puzzle2(self):
        raw_input = read_file('input_day05')
        lines = [parse_line(raw_line) for raw_line in raw_input]
        number_of_multiple_occurrences = calculate_multiple_occurrences(lines)
        self.assertEqual(16925, number_of_multiple_occurrences)
