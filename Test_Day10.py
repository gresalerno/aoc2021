from unittest import TestCase

from file_helper import read_file

from Day10 import find_illegal_character, calculate_syntax_error_score, calculate_autocomplete_score


class TestDay10(TestCase):
    def test_find(self):
        line = '([])'
        self.assertEqual(None, find_illegal_character(line, []))
        line = '([]}'
        self.assertEqual('}', find_illegal_character(line, []))
        line = '{([(<{}[<>[]}>{[]{[(<()>'
        self.assertEqual('}', find_illegal_character(line, []))
        lines = ['[({(<(())[]>[[{[]{<()<>>',
                 '[(()[<>])]({[<{<<[]>>(',
                 '{([(<{}[<>[]}>{[]{[(<()>',
                 '(((({<>}<{<{<>}{[]{[]{}',
                 '[[<[([]))<([[{}[[()]]]',
                 '[{[{({}]{}}([{[{{{}}([]',
                 '{<[[]]>}<{[{[{[]{()[[[]',
                 '[<(<(<(<{}))><([]([]()',
                 '<{([([[(<>()){}]>(<<{{',
                 '<{([{{}}[<[[[<>{}]]]>[]]']
        total_score = 0
        for line in lines:
            total_score += calculate_syntax_error_score(line)
        self.assertEqual(26397, total_score)

    def test_puzzle1(self):
        lines = read_file('input_day10')
        total_score = 0
        for line in lines:
            total_score += calculate_syntax_error_score(line)
        self.assertEqual(323613, total_score)

    def test_complete_line(self):
        line = '[({(<(())[]>[[{[]{<()<>>'
        self.assertEqual(288957, calculate_autocomplete_score(line))
        line = '[(()[<>])]({[<{<<[]>>('
        self.assertEqual(5566, calculate_autocomplete_score(line))
        line = '(((({<>}<{<{<>}{[]{[]{}'
        self.assertEqual(1480781, calculate_autocomplete_score(line))
        line = '{<[[]]>}<{[{[{[]{()[[[]'
        self.assertEqual(995444, calculate_autocomplete_score(line))
        line = '<{([{{}}[<[[[<>{}]]]>[]]'
        self.assertEqual(294, calculate_autocomplete_score(line))

    def test_middle_score(self):
        lines = ['[({(<(())[]>[[{[]{<()<>>',
                 '[(()[<>])]({[<{<<[]>>(',
                 '{([(<{}[<>[]}>{[]{[(<()>',
                 '(((({<>}<{<{<>}{[]{[]{}',
                 '[[<[([]))<([[{}[[()]]]',
                 '[{[{({}]{}}([{[{{{}}([]',
                 '{<[[]]>}<{[{[{[]{()[[[]',
                 '[<(<(<(<{}))><([]([]()',
                 '<{([([[(<>()){}]>(<<{{',
                 '<{([{{}}[<[[[<>{}]]]>[]]']
        score = []
        for line in lines:
            s = calculate_autocomplete_score(line)
            if s is not None:
                score.append(s)
        score.sort()
        middle_score = score[len(score)//2]
        self.assertEqual(288957, middle_score)

    def test_puzzle2(self):
        lines = read_file('input_day10')
        score = []
        for line in lines:
            s = calculate_autocomplete_score(line)
            if s is not None:
                score.append(s)
        score.sort()
        middle_score = score[len(score) // 2]
        self.assertEqual(3103006161, middle_score)
