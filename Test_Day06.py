from unittest import TestCase

from file_helper import read_file

from Day06 import evolve


class TestDay06(TestCase):
    def test(self):
        population = [3, 4, 3, 1, 2]
        self.assertEqual(5934, sum(evolve(population, 80).values()))
        population_puzzle = list(map(int, read_file('input_day06')[0].split(',')))
        self.assertEqual(388419, sum(evolve(population_puzzle, 80).values()))
        self.assertEqual(1740449478328, sum(evolve(population_puzzle, 256).values()))
