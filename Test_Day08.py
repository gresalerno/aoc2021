from unittest import TestCase

from file_helper import read_file

from Day08 import count_easy_digits, solve_cipher, seven_segments_search


class TestDay08(TestCase):
    def test_puzzle1(self):
        number_of_easy_digits = sum([count_easy_digits(four_digit) for four_digit in read_file('input_day08')])
        self.assertEqual(543, number_of_easy_digits)

    def test_cipher_and_the_rest(self):
        list_of_numbers = ['acedgfb', 'cdfbe' 'gcdfa' 'fbcad', 'dab', 'cefabd', 'cdfgeb', 'eafb', 'cagedb', 'ab']
        self.assertEqual({'d': 'a', 'e': 'b', 'a': 'c', 'f': 'd', 'g': 'e', 'b': 'f', 'c': 'g'},
                         solve_cipher(list_of_numbers))
        self.assertEqual(5, seven_segments_search(list_of_numbers, ['cdfeb']))
        self.assertEqual(5353, seven_segments_search(list_of_numbers, ['cdfeb', 'fcadb', 'cdfeb', 'cdbaf']))

    def test_puzzle2(self):
        four_digits_output_values = 0
        for line in read_file('input_day08'):
            list_of_numbers = line.split(' | ')[0].split()
            four_digits = line.split(' | ')[1].split()
            four_digits_output_values += seven_segments_search(list_of_numbers, four_digits)
        self.assertEqual(994266, four_digits_output_values)
