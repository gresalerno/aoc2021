from unittest import TestCase

from file_helper import read_file

from Day09 import parse_array, low_points, low_points_coordinates, risk_level, find_basin


class TestDay09(TestCase):
    def test_(self):
        height_map = ['2199943210', '3987894921', '9856789892', '8767896789', '9899965678']
        self.assertEqual(10, len(parse_array(height_map)[0]))
        self.assertEqual([1, 5, 5, 0], low_points(parse_array(height_map)))
        self.assertEqual(15, risk_level(low_points(parse_array(height_map))))

    def test_puzzle1(self):
        height_map = read_file('input_day09')
        self.assertEqual(425, risk_level(low_points(parse_array(height_map))))

    def test_basin(self):
        height_map = ['2199943210', '3987894921', '9856789892', '8767896789', '9899965678']
        parsed_height_map = parse_array(height_map)
        self.assertEqual([(1, 0), (2, 2), (6, 4), (9, 0)], low_points_coordinates(parsed_height_map))
        low_point_coordinates = (1, 0)
        self.assertEqual([(1, 0), (0, 0), (0, 1)], find_basin(low_point_coordinates, parsed_height_map, []))
        low_point_coordinate = (2, 2)
        self.assertEqual(5, parsed_height_map[low_point_coordinate[1]][low_point_coordinate[0]])
        self.assertEqual(14, len(find_basin(low_point_coordinate, parsed_height_map, [])))
        low_point_coordinate = (6, 4)
        self.assertEqual(5, parsed_height_map[low_point_coordinate[1]][low_point_coordinate[0]])
        self.assertEqual(9, len(find_basin(low_point_coordinate, parsed_height_map, [])))
        low_point_coordinate = (9, 0)
        self.assertEqual(0, parsed_height_map[low_point_coordinate[1]][low_point_coordinate[0]])
        self.assertEqual(9, len(find_basin(low_point_coordinate, parsed_height_map, [])))
        basins = [len(find_basin(lp, parsed_height_map, [])) for lp in low_points_coordinates(parsed_height_map)]
        self.assertEqual([9, 9, 14], sorted(basins)[-3:])
        highest_basins = sorted(basins)[-3:]
        self.assertEqual(1134, highest_basins[0]*highest_basins[1]*highest_basins[2])

    def test_puzzle2(self):
        height_map = read_file('input_day09')
        parsed_height_map = parse_array(height_map)
        basins = [len(find_basin(lp, parsed_height_map, [])) for lp in low_points_coordinates(parsed_height_map)]
        highest_basins = sorted(basins)[-3:]
        self.assertEqual(1135260, highest_basins[0]*highest_basins[1]*highest_basins[2])

