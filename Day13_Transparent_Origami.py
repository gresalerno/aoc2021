class TransparentPaper:
    def __init__(self, points, max_x, max_y):
        self.points = points
        self.max_x = max_x
        self.max_y = max_y

    def __str__(self):
        paper = ''
        for j in range(self.max_y+1):
            line = ''
            for i in range(self.max_x+1):
                if (i, j) in self.points:
                    line += '#'
                else:
                    line += '.'
            paper += line + '\n'
        return paper

    def fold_horizontally(self, axes):
        upper_points = [point for point in self.points if point[1] < axes]
        lower_points = [(point[0], self.max_y - point[1]) for point in self.points if point[1] > axes]
        if axes > self.max_y//2:
            new_max = axes - 1
            shift = new_max - (self.max_y - axes) + 1
            lower_points = [(point[0], point[1] + shift) for point in lower_points]
        else:
            new_max = self.max_y - axes - 1
            shift = new_max - axes + 1
            upper_points = [(point[0], point[1] + shift) for point in upper_points]
        self.points = set(upper_points + lower_points)
        self.max_y = new_max

    def fold_vertically(self, axes):
        left_points = [point for point in self.points if point[0] < axes]
        right_points = [(self.max_x - point[0], point[1]) for point in self.points if point[0] > axes]
        if axes > self.max_x//2:
            new_max = axes - 1
            shift = new_max - (self.max_x - axes) + 1
            right_points = [(point[0] + shift, point[1]) for point in right_points]
        else:
            new_max = self.max_x - axes - 1
            shift = new_max - axes + 1
            left_points = [(point[0] + shift, point[1]) for point in left_points]
        self.points = set(left_points + right_points)
        self.max_x = new_max


def parse_folding(transparent_paper):
    points = set()
    axes_for_folding = []
    value_for_folding = []
    for line in transparent_paper:
        splitted = line.split(',')
        if len(splitted) == 2:
            points.add((int(splitted[0]), int(splitted[1])))
        if len(line.split(' ')) == 3:
            axes_for_folding.append(line.split(' ')[2].split('=')[0])
            value_for_folding.append(int(line.split(' ')[2].split('=')[1]))
    return points, axes_for_folding, value_for_folding
