from unittest import TestCase

from file_helper import read_file

from Day22 import parse_input, operate_reboot, Volume


class TestDay22(TestCase):
    def test_simple_input(self):
        reboot_steps = ['on x=10..12,y=10..12,z=10..12',
                        'on x=11..13,y=11..13,z=11..13',
                        'off x=9..11,y=9..11,z=9..11',
                        'on x=10..10,y=10..10,z=10..10']
        initialization = True
        reboot_switch, limits = parse_input(reboot_steps[0], initialization)
        self.assertEqual('on', reboot_switch)
        self.assertEqual(limits, [[10, 12], [10, 12], [10, 12]])
        reboot_switch, limits = parse_input(reboot_steps[2], initialization)
        self.assertEqual('off', reboot_switch)
        self.assertEqual(limits, [[9, 11], [9, 11], [9, 11]])
        already_on = set([])
        for reboot_step in reboot_steps:
            reboot_switch, limits = parse_input(reboot_step, initialization)
            already_on = operate_reboot(reboot_switch, limits, already_on)
        self.assertEqual(39, len(already_on))

    def test_larger_input(self):
        reboot_steps = read_file('input_test_day22')
        already_on = set([])
        initialization = True
        for reboot_step in reboot_steps:
            reboot_switch, limits = parse_input(reboot_step, initialization)
            already_on = operate_reboot(reboot_switch, limits, already_on)
        self.assertEqual(590784, len(already_on))

    def test_puzzle1(self):
        reboot_steps = read_file('input_day22')
        initialization = True
        already_on = set([])
        for reboot_step in reboot_steps:
            reboot_switch, limits = parse_input(reboot_step, initialization)
            already_on = operate_reboot(reboot_switch, limits, already_on)
        self.assertEqual(576028, len(already_on))

    def test_calculate_volumes(self):
        reboot_steps = read_file('input_test2_day22')
        areas_on = []
        for reboot_step in reboot_steps:
            reboot_switch, limits = parse_input(reboot_step, False)
            v = Volume(limits[0], limits[1], limits[2])
            for vv in areas_on:
                vv.subtract(v)
            if reboot_switch == 'on':
                areas_on.append(v)
        self.assertEqual(2758514936282235, sum([v.volume() for v in areas_on]))

    def test_puzzle2(self):
        reboot_steps = read_file('input_day22')
        areas_on = []
        for reboot_step in reboot_steps:
            reboot_switch, limits = parse_input(reboot_step, False)
            v = Volume(limits[0], limits[1], limits[2])
            for vv in areas_on:
                vv.subtract(v)
            if reboot_switch == 'on':
                areas_on.append(v)
        self.assertEqual(1387966280636636, sum([v.volume() for v in areas_on]))