from unittest import TestCase

from Day01 import increase_depths_measures, increase_three_measurement

from file_helper import read_file


class TestDay01(TestCase):
    def test_(self):
        depths = [199, 200, 208, 210]
        self.assertEqual([200, 208, 210], depths[1:])
        self.assertEqual([(199, 200), (200, 208), (208, 210)], list(zip(depths, depths[1:])))
        self.assertEqual([(199, 200), (200, 208), (208, 210)],
                         list(filter(lambda t: t[0] < t[1], zip(depths, depths[1:]))))
        self.assertEqual(3, len(list(filter(lambda t: t[0] < t[1], zip(depths, depths[1:])))))
        self.assertEqual(3, increase_depths_measures(depths))
        depths_2 = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
        self.assertEqual(7, increase_depths_measures(depths_2))
        depths_puzzle = list(map(int, read_file('input_day01')))
        self.assertEqual(1559, increase_depths_measures(depths_puzzle))
        self.assertEqual(5, increase_three_measurement(depths_2))
        self.assertEqual(1600, increase_three_measurement(depths_puzzle))
