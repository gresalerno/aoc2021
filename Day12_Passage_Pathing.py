from collections import defaultdict


def parse_caves(raw_caves):
    caves = defaultdict(lambda: [])
    for line in raw_caves:
        vertexes = line.split('-')
        caves[vertexes[0]].append(vertexes[1])
        caves[vertexes[1]].append(vertexes[0])
    return caves


def explore_caves(caves, paths, current_path, current_vertex, path_filter=lambda cv, cp: cv.islower() and cv in cp):
    if current_vertex == 'end':
        paths.append(current_path)
        return
    if path_filter(current_vertex, current_path):
        return
    if current_vertex.islower():
        current_path[current_vertex] += 1
    possible_new_vertexes = caves[current_vertex]
    for possible_new_vertex in possible_new_vertexes:
        new_current_path = current_path.copy()
        explore_caves(caves, paths, new_current_path, possible_new_vertex, path_filter)


def path_filter(current_vertex, current_path):
    if current_path['start'] == 2:
        return True
    return current_path[current_vertex] >= 1 and 2 in current_path.values()



