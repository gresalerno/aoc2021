import numpy as np
from collections import Counter
from itertools import product


class Scanner:
    def __init__(self, beacons_original_positions):
        self.beacons_original_positions = beacons_original_positions
        self.beacons_aligned_positions = None
        self.absolute_position = None

    def beacons_positions(self):
        return [add(self.absolute_position, beacon) for beacon in self.beacons_aligned_positions]


def parse_input(raw_input):
    scanners = []
    for raw_scanner in raw_input:
        scanner = []
        for scan in raw_scanner[1:]:
            x, y, z = scan.split(',')
            scanner.append([int(x), int(y), int(z)])
        scanners.append(Scanner(scanner))
    scanners[0].beacons_aligned_positions = scanners[0].beacons_original_positions
    scanners[0].absolute_position = (0, 0, 0)
    return scanners


def add(point1, point2):
    return [x1 + x2 for (x1, x2) in zip(point1, point2)]


def difference(point1, point2):
    return [x1 - x2 for (x1, x2) in zip(point1, point2)]


def multiply(point1, point2):
    return [x1 * x2 for (x1, x2) in zip(point1, point2)]


def distance(point1, point2):
    x1, y1, z1 = point1
    x2, y2, z2 = point2
    return np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2)


def find_scanner_origin(scanner0, scanner, number_for_area, debug=False):
    all_rotated_scanners = get_all_rotated_scanners(scanner.beacons_original_positions)
    for rotated_scanner in all_rotated_scanners:
        candidate_origin = []
        for point0 in scanner0.beacons_aligned_positions:
            for point in rotated_scanner:
                candidate_origin.append(tuple(difference(point0, point)))
        all_distances = Counter(candidate_origin)
        if debug:
            print(all_distances)
        origin = max(all_distances, key=all_distances.get)
        if all_distances[origin] >= number_for_area:
            scanner.absolute_position = add(scanner0.absolute_position, origin)
            scanner.beacons_aligned_positions = rotated_scanner
            return list(origin)


def rotate(xyzscanner):
    all_rotations = []
    axis_rotations = list(product((1, -1), repeat=3))
    order = [[0, 1, 2], [1, 0, 2], [2, 0, 1]]
    scanner = np.zeros((len(xyzscanner), 3))
    for r in range(3):
        scanner[:, 0] = np.array(xyzscanner)[:, order[r][0]]
        scanner[:, 1] = np.array(xyzscanner)[:, order[r][1]]
        scanner[:, 2] = np.array(xyzscanner)[:, order[r][2]]
        for i in range(8):
            rotated_scanner = []
            for point in scanner:
                rotated_scanner.append(multiply(axis_rotations[i], point))
            all_rotations.append(rotated_scanner)
    return all_rotations


def swap_axes(scanner):
    return [
        scanner,
        scanner[1]
    ]


def get_rotations(x, y, z):
    return [
        (x, y, z),
        (-y, x, z),
        (-x, -y, z),
        (y, -x, z),
    ]


def get_z_orientations(x, y, z):
    return [
        (x, y, z),
        (x, z, -y),
        (x, -y, -z),
        (x, -z, y),
        (-z, y, x),
        (z, y, -x),
    ]


def get_orientations(point):
    x, y, z = point
    for xi, yi, zi in get_z_orientations(x, y, z):
        yield from get_rotations(xi, yi, zi)


def get_all_rotated_scanners(scanner):
    rotations = []
    for beacon in scanner:
        orientations = get_orientations(beacon)
        for index, o in enumerate(orientations):
            if len(rotations) < index + 1:
                rotations.append([])
            rotations[index].append(o)
    return rotations


def calculate_max_distance(scanners):
    max_distance = 0
    for s1 in scanners:
        for s2 in scanners:
            x, y, z = difference(s1.absolute_position, s2.absolute_position)
            distance = abs(x) + abs(y) + abs(z)
            if distance > max_distance:
                max_distance = distance
    return max_distance


def find_scanners_positions(scanners):
    if all(scanner.absolute_position is not None for scanner in scanners):
        return
    already_placed_scanners = [scanner for scanner in scanners if scanner.absolute_position is not None]
    for placed_scanner in already_placed_scanners:
        scanner_to_place = [scanner for scanner in scanners if scanner.absolute_position is None]
        for scanner in scanner_to_place:
            find_scanner_origin(placed_scanner, scanner, 12)
    return find_scanners_positions(scanners)


def find_unique_beacons(scanners):
    beacons = set()
    for scanner in scanners:
        for b in scanner.beacons_positions():
            beacons.add(tuple(b))
    unique_beacons = len(beacons)
    return unique_beacons
