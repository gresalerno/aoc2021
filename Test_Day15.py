from unittest import TestCase

from file_helper import read_file

from Day15 import parse_input, create_bigger_risk_level_map, create_graph, move


class TestDay15(TestCase):
    def test_simple_input(self):
        raw_input = ['1163751742',
                     '1381373672',
                     '2136511328',
                     '3694931569',
                     '7463417111',
                     '1319128137',
                     '1359912421',
                     '3125421639',
                     '1293138521',
                     '2311944581']
        risk_level_map = parse_input(raw_input)
        graph, lx, ly = create_graph(risk_level_map)
        risk_level = move(graph, lx, ly)
        self.assertEqual(40, risk_level[(lx-1, ly-1)])

    def test_puzzle1(self):
        raw_input = read_file('input_day15')
        risk_level_map = parse_input(raw_input)
        graph, lx, ly = create_graph(risk_level_map)
        risk_level = move(graph, lx, ly)
        self.assertEqual(613, risk_level[(lx - 1, ly - 1)])

    def test_bigger_matrix(self):
        raw_input = ['1163751742',
                     '1381373672',
                     '2136511328',
                     '3694931569',
                     '7463417111',
                     '1319128137',
                     '1359912421',
                     '3125421639',
                     '1293138521',
                     '2311944581']
        risk_level_map = parse_input(raw_input)
        bigger_risk_level_map = create_bigger_risk_level_map(risk_level_map)
        graph, lx, ly = create_graph(bigger_risk_level_map)
        risk_level = move(graph, lx, ly)
        self.assertEqual(315, risk_level[(lx - 1, ly - 1)])

    def test_puzzle2(self):
        raw_input = read_file('input_day15')
        risk_level_map = parse_input(raw_input)
        bigger_risk_level_map = create_bigger_risk_level_map(risk_level_map)
        graph, lx, ly = create_graph(bigger_risk_level_map)
        risk_level = move(graph, lx, ly)
        self.assertEqual(2899, risk_level[(lx - 1, ly - 1)])