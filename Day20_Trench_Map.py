def parse_image(raw_image):
    algorithm = raw_image[0]
    image = set()
    for y, row in enumerate(raw_image[2:]):
        for x, point in enumerate(row):
            if point == '#':
                image.add((x, y))
    max_x = len(raw_image[2:][0])
    max_y = len(raw_image[2:])
    return algorithm, image, max_x, max_y


def tick(algorithm, image, max_x, max_y, offset):
    new_image = set()
    for y in range(-offset, max_y + offset):
        for x in range(-offset, max_x + offset):
            output_pixel = ''
            for n in neighbours(x, y):
                if n in image:
                    output_pixel += '1'
                else:
                    output_pixel += '0'
            index = int(output_pixel, 2)
            if algorithm[index] == '#':
                new_image.add((x, y))
    return new_image


def neighbours(x, y):
    return [(x-1, y-1), (x, y-1), (x+1, y-1), (x-1, y), (x, y), (x+1, y), (x-1, y+1), (x, y+1), (x+1, y+1)]