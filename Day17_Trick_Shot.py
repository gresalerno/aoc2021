from itertools import product

import numpy as np

def parse_input(raw_input):
    rawlimits = raw_input.split(': ')[1].split(', ')
    xlimits = rawlimits[0].split('=')
    ylimits = rawlimits[1].split('=')
    xmin = xlimits[1].split('..')[0]
    xmax = xlimits[1].split('..')[1]
    ymin = ylimits[1].split('..')[0]
    ymax = ylimits[1].split('..')[1]
    return int(xmin), int(xmax), int(ymin), int(ymax)


def calculate_new_position(x, y, vx, vy):
    new_x = x + vx
    new_y = y + vy
    new_vx = np.sign(vx)*(abs(vx) - 1)
    new_vy = vy - 1
    return new_x, new_y, new_vx, new_vy


def calculate_valid_velocities(x_min, x_max, y_min, y_max):
    highest_y = {}
    valid_velocities = set()
    vx_range = range(1, 2*x_max)
    vy_range = range(y_min, -y_min)
    for vx, vy in product(vx_range, vy_range):
        initial_velocity = (vx, vy)
        x, y = 0, 0
        highest_y[initial_velocity] = float('-inf')
        while x <= x_max and y >= y_min:
            if x_min <= x <= x_max and y_min <= y <= y_max:
                valid_velocities.add(initial_velocity)
            if y > highest_y[initial_velocity]:
                highest_y[initial_velocity] = y
            x, y, vx, vy = calculate_new_position(x, y, vx, vy)
    return max([highest_y[initial_velocity] for initial_velocity in valid_velocities]), len(valid_velocities)
