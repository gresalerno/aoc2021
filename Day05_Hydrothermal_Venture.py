import collections


class Line:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def all_points(self):
        first_point = min(self.start, self.end)
        last_point = max(self.start, self.end)
        if self.is_vertical():
            delta = abs(last_point[1] - first_point[1])
            return [(first_point[0], first_point[1]+i) for i in range(delta+1)]
        elif self.is_horizontal():
            delta = abs(last_point[0] - first_point[0])
            return [(first_point[0]+i, first_point[1]) for i in range(delta+1)]
        else:
            delta = abs(last_point[1] - first_point[1])
            if last_point[1] > first_point[1]:
                return [(first_point[0]+i, first_point[1]+i) for i in range(delta+1)]
            else:
                return [(first_point[0]+i, first_point[1]-i) for i in range(delta+1)]

    def is_vertical(self):
        return self.start[0] == self.end[0]

    def is_horizontal(self):
        return self.start[1] == self.end[1]

    def is_horizontal_or_vertical(self):
        return self.is_vertical() or self.is_horizontal()

    def __str__(self):
        return f"{self.start} -> {self.end}"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.start == other.start and self.end == other.end


def calculate_multiple_occurrences(lines):
    points = []
    for line in lines:
        points += line.all_points()
    points_occurrences = collections.Counter(points)
    number_of_multiple_occurrences = len(
        [occurrence for occurrence in points_occurrences.values() if occurrence > 1])
    return number_of_multiple_occurrences


def calculate_multiple_occurrences_in_horizontal_or_vertical_lines(lines):
    horizontal_or_vertical_lines = [line for line in lines if line.is_horizontal_or_vertical()]
    return calculate_multiple_occurrences(horizontal_or_vertical_lines)


def parse_line(raw_line):
    raw_points = raw_line.split(' -> ')
    raw_start = raw_points[0].split(',')
    raw_end = raw_points[1].split(',')
    return Line((int(raw_start[0]), int(raw_start[1])), (int(raw_end[0]), int(raw_end[1])))
