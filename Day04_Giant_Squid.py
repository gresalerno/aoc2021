class Board:
    def __init__(self, board):
        self.board = list(board)

    def __getitem__(self, item):
        return self.board[item]

    def extract_number(self, number):
        for row in self.board:
            for index, tile in enumerate(row):
                if row[index] == number:
                    row[index] = 'X'

    def has_won(self):
        for index in range(5):
            column = [row[index] for row in self.board]
            if is_completed(self.board[index]) or is_completed(column):
                return True

    def calculate_sum_of_unmarked_numbers(self):
        sum_of_unmarked_numbers = 0
        for row in self.board:
            for tile in row:
                if tile != 'X':
                    sum_of_unmarked_numbers += tile
        return sum_of_unmarked_numbers


def score(boards, drawn_numbers):
    for number in drawn_numbers:
        for board in boards:
            board.extract_number(number)
            if board.has_won():
                return number*board.calculate_sum_of_unmarked_numbers()
    raise Exception('No Winner')


def score_of_last(boards, drawn_numbers):
    winning_boards = set()
    for number in drawn_numbers:
        for board in boards:
            board.extract_number(number)
            if board.has_won():
                winning_boards.add(board)
            if len(winning_boards) == len(boards):
                return number*board.calculate_sum_of_unmarked_numbers()
    raise Exception('No Winner')


def is_completed(line):
    return all(tile == 'X' for tile in line)
