from unittest import TestCase

from file_helper import read_file

from Day13 import parse_folding, TransparentPaper


class TestDay13(TestCase):
    def test_parse(self):
        transparent_papers = ['6, 10',
                              '0, 14',
                              '9, 10',
                              '0, 3',
                              '10, 4',
                              '4, 11',
                              '6, 0',
                              '6, 12',
                              '4, 1',
                              '0, 13',
                              '10, 12',
                              '3, 4',
                              '3, 0',
                              '8, 4',
                              '1, 10',
                              '2, 14',
                              '8, 10',
                              '9, 0',
                              '',
                              'fold along y=7',
                              'fold along x=5']
        points, axes_for_folding, value_for_folding = parse_folding(transparent_papers)
        self.assertEqual(18, len(points))
        self.assertEqual(['y', 'x'], axes_for_folding)
        self.assertEqual([7, 5], value_for_folding)

    def test_print(self):
        transparent_papers = ['6,10',
                              '0,14',
                              '9,10',
                              '0,3',
                              '10,4',
                              '4,11',
                              '6,0',
                              '6,12',
                              '4,1',
                              '0,13',
                              '10,12',
                              '3,4',
                              '3,0',
                              '8,4',
                              '1,10',
                              '2,14',
                              '8,10',
                              '9,0',
                              '',
                              'fold along y=7',
                              'fold along x=5']
        points, axes_for_folding, value_for_folding = parse_folding(transparent_papers)
        max_x = max(points, key=lambda p: p[0])[0]
        max_y = max(points, key=lambda p: p[1])[1]
        paper = TransparentPaper(points, max_x, max_y)
        for direction, axes in zip(axes_for_folding, value_for_folding):
            if direction == 'y':
                paper.fold_horizontally(axes)
            else:
                paper.fold_vertically(axes)
        print(paper)

    def test_puzzle1(self):
        raw = read_file('input_day13')
        points, axes_for_folding, value_for_folding = parse_folding(raw)
        max_x = max(points, key=lambda p: p[0])[0]
        max_y = max(points, key=lambda p: p[1])[1]
        paper = TransparentPaper(points, max_x, max_y)
        direction = axes_for_folding[0]
        axes = value_for_folding[0]
        if direction == 'y':
            paper.fold_horizontally(axes)
        else:
            paper.fold_vertically(axes)
        self.assertEqual(689, len(paper.points))

    def test_puzzle2(self):
        raw = read_file('input_day13')
        points, axes_for_folding, value_for_folding = parse_folding(raw)
        max_x = max(points, key=lambda p: p[0])[0]
        max_y = max(points, key=lambda p: p[1])[1]
        paper = TransparentPaper(points, max_x, max_y)
        for direction, axes in zip(axes_for_folding, value_for_folding):
            if direction == 'y':
                paper.fold_horizontally(axes)
            else:
                paper.fold_vertically(axes)
        print(paper)
        # RLBCJGLU
